package com.dlabs.imob.inventory.core;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

public class ImobMapper {

    public static ModelMapper modelMapper;

    static {
        modelMapper = new ModelMapper();
        modelMapper.getConfiguration()
                .setSkipNullEnabled(true)
                .setMatchingStrategy(MatchingStrategies.LOOSE);
    }

}
