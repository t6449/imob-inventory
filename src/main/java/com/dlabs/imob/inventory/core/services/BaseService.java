package com.dlabs.imob.inventory.core.services;

import com.dlabs.imob.inventory.core.models.BaseModel;
import com.dlabs.imob.inventory.core.repositories.BaseRepository;

public interface BaseService<
        R extends BaseRepository<E, I>,
        E extends BaseModel<I>,
        I
        > {

    R getRepository();

}
