package com.dlabs.imob.inventory.core.controllers;

import com.dlabs.imob.inventory.core.dtos.ResponseDto;
import com.dlabs.imob.inventory.core.models.BaseModel;
import com.dlabs.imob.inventory.core.repositories.BaseRepository;
import com.dlabs.imob.inventory.core.services.SearchableService;
import com.dlabs.imob.inventory.domain.property.PropertySpecification;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

public interface SearchableController<
        S extends SearchableService<R, E, ID>,
        R extends BaseRepository<E, ID>,
        E extends BaseModel<ID>,
        ID
        > {

    S getService();

    @GetMapping("/{id}")
    default ResponseDto findById(@PathVariable ID id) {
        return this.getService().findById(id);
    }

    @GetMapping("")
    default ResponseDto findAll() {
        return this.getService().findAll();
    }

    @GetMapping("/page")
    default ResponseDto getPage( Pageable pageable, PropertySpecification specification ) {
        return this.getService().getPage(pageable, specification);
    }

}
