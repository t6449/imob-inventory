package com.dlabs.imob.inventory.domain.classification;

import com.dlabs.imob.inventory.core.controllers.SearchableController;
import com.dlabs.imob.inventory.core.dtos.ResponseDto;
import com.dlabs.imob.inventory.shared.Constants;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(Constants.BASE_URL + "/classifications")
@Getter
public class ClassificationController implements SearchableController<
        ClassificationService,
        ClassificationRepository,
        ClassificationModel,
        Long
        > {

    @Autowired
    private ClassificationService service;

    @GetMapping("/with-property-types")
    public ResponseDto findAll() {

        return this.service.findAll();

    }

}
