package com.dlabs.imob.inventory.domain.property;

import com.dlabs.imob.inventory.core.BusinessException;
import com.dlabs.imob.inventory.core.dtos.ResponseDto;
import com.dlabs.imob.inventory.core.services.DeletableService;
import com.dlabs.imob.inventory.core.services.SearchableService;
import com.dlabs.imob.inventory.external.drive.DriveService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(rollbackFor = Exception.class)
@Getter
public class PropertyService implements
        SearchableService<
            PropertyRepository,
            PropertyModel,
            Long
        >,
        DeletableService <
            PropertyRepository,
            PropertyModel,
            Long
        > {

    @Autowired
    private PropertyRepository repository;

    @Autowired
    private DriveService driveService;

    public ResponseDto createByDto(PropertyInDto dto) {

        this.driveService.checkFileExistById(dto.getFileId());

        PropertyModel model = this.repository.save(
                PropertyModel.builder()
                        .title(dto.getTitle())
                        .fileId(dto.getFileId())
                        .address(dto.getAddress())
                        .description(dto.getDescription())
                        .surface(dto.getSurface())
                        .build()
        );

        this.repository.save(model);

        return ResponseDto.builder()
                .data(model)
                .build();
    }

    public ResponseDto updateByDto(PropertyInDto dto, Long id) {

        if (!this.repository.existsById(id)) {
            throw new BusinessException("Property id not exist.");
        }
        this.driveService.checkFileExistById(dto.getFileId());

        PropertyModel model = this.repository.findById(id).orElse(new PropertyModel());
        model.setTitle(dto.getTitle());
        model.setFileId(dto.getFileId());
        model.setAddress(dto.getAddress());
        model.setDescription(dto.getDescription());
        model.setSurface(dto.getSurface());

        this.repository.save(model);

        return ResponseDto.builder()
                .data(model)
                .build();
    }

}
