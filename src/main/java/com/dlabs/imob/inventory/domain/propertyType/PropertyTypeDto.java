package com.dlabs.imob.inventory.domain.propertyType;

import com.dlabs.imob.inventory.core.dtos.OutDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class PropertyTypeDto extends OutDto {

    private String name;

    private Long id;

}
