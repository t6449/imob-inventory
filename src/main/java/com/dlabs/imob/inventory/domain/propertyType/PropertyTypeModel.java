package com.dlabs.imob.inventory.domain.propertyType;

import com.dlabs.imob.inventory.core.models.LongBaseModel;
import lombok.*;
import lombok.experimental.SuperBuilder;
import javax.persistence.*;

@Entity
@Table(name = "property_type")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class PropertyTypeModel extends LongBaseModel {

    @Column(name = "name")
    private String name;

}
