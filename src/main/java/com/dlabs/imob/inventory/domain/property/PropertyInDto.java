package com.dlabs.imob.inventory.domain.property;

import com.dlabs.imob.inventory.core.dtos.InDto;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;

@Builder
@Data
@EqualsAndHashCode(callSuper = false)
public class PropertyInDto extends InDto {

    @NotBlank(message = "title is required.")
    private String title;

    @NotBlank(message = "file id is required.")
    private String fileId;

    private String address;

    private String surface;

    private String description;

}


