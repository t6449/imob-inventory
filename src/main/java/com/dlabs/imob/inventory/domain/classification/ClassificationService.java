package com.dlabs.imob.inventory.domain.classification;

import com.dlabs.imob.inventory.core.BusinessException;
import com.dlabs.imob.inventory.core.dtos.ResponseDto;
import com.dlabs.imob.inventory.core.services.SearchableService;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
@Getter
public class ClassificationService implements SearchableService<
        ClassificationRepository,
        ClassificationModel,
        Long
        > {

    @Autowired
    private ClassificationRepository repository;

}
