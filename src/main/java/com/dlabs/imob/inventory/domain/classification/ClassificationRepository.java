package com.dlabs.imob.inventory.domain.classification;

import com.dlabs.imob.inventory.core.repositories.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClassificationRepository extends
        BaseRepository<ClassificationModel, Long> {
}

