package com.dlabs.imob.inventory.domain.property;

import com.dlabs.imob.inventory.core.controllers.DeletableController;
import com.dlabs.imob.inventory.core.controllers.SearchableController;
import com.dlabs.imob.inventory.core.dtos.ResponseDto;
import com.dlabs.imob.inventory.shared.Constants;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(Constants.BASE_URL + "/properties")
@Getter
public class PropertyController implements
        SearchableController<
            PropertyService,
            PropertyRepository,
            PropertyModel,
            Long
        >,
        DeletableController<
            PropertyService,
            PropertyRepository,
            PropertyModel,
            Long
        > {

    @Autowired
    private PropertyService service;

    @PostMapping("")
    public ResponseDto create(@RequestBody @Valid PropertyInDto dto) {
        return this.service.createByDto(dto);
    }

    @PutMapping("/{id}")
    public ResponseDto update(@RequestBody @Valid PropertyInDto dto, @PathVariable Long id) {
        return this.service.updateByDto(dto, id);
    }

}
