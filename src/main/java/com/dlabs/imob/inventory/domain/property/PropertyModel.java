package com.dlabs.imob.inventory.domain.property;

import com.dlabs.imob.inventory.core.models.LongBaseModel;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Table(name = "property")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@Where(clause = "deleted_at IS NULL")
public class PropertyModel extends LongBaseModel {

    @Column(name = "title")
    private String title;

    @Column(name = "file_id")
    private String fileId;

    @Column(name = "address")
    private String address;

    @Column(name = "surface")
    private String surface;

    @Column(name = "description")
    private String description;

}
