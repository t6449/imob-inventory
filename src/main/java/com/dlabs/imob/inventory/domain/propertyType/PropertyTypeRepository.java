package com.dlabs.imob.inventory.domain.propertyType;

import com.dlabs.imob.inventory.core.repositories.BaseRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyTypeRepository extends BaseRepository<PropertyTypeModel, Long> {
}

