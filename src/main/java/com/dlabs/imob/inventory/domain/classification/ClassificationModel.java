package com.dlabs.imob.inventory.domain.classification;

import com.dlabs.imob.inventory.core.models.LongBaseModel;
import com.dlabs.imob.inventory.domain.propertyType.PropertyTypeModel;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "classification")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@SuperBuilder
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class  ClassificationModel extends LongBaseModel {

    @Column(name = "name")
    private String name;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "classification_property_type",
            joinColumns = @JoinColumn(
                    name = "classification_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "property_type_id", referencedColumnName = "id"))
    private Set<PropertyTypeModel> propertyTypes;

}

