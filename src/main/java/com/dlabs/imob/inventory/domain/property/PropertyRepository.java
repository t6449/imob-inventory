package com.dlabs.imob.inventory.domain.property;

import com.dlabs.imob.inventory.core.repositories.DeletableRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PropertyRepository extends
        DeletableRepository<
                PropertyModel,
                Long
                > {
}

