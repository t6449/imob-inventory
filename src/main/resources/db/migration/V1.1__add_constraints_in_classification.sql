ALTER TABLE inventory.classification
    ADD CONSTRAINT PK_classification PRIMARY KEY (id),
    ADD CONSTRAINT UQ_name_classification UNIQUE (name);
