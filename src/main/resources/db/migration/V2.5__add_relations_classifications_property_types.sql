INSERT INTO inventory.classification_property_type (classification_id, property_type_id)
VALUES
       ( (select id from inventory.classification where name = 'Habitacional') , (select id from inventory.property_type where name = 'Casas') ),
       ( (select id from inventory.classification where name = 'Habitacional') , (select id from inventory.property_type where name = 'Departamentos') ),
       ( (select id from inventory.classification where name = 'Habitacional') , (select id from inventory.property_type where name = 'Terrenos') ),
       ( (select id from inventory.classification where name = 'Habitacional') , (select id from inventory.property_type where name = 'Otros') ),

       ( (select id from inventory.classification where name = 'Comercial') , (select id from inventory.property_type where name = 'Terrenos') ),
       ( (select id from inventory.classification where name = 'Comercial') , (select id from inventory.property_type where name = 'Otros') ),
       ( (select id from inventory.classification where name = 'Comercial') , (select id from inventory.property_type where name = 'Locales') ),
       ( (select id from inventory.classification where name = 'Comercial') , (select id from inventory.property_type where name = 'Oficinas') )

;
