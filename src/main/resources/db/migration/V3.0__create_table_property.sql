CREATE TABLE inventory.property
(
    -- Base
    id                BIGSERIAL    NOT NULL,

    -- Auditory
    created_by        VARCHAR(256) NOT NULL DEFAULT 'SYSTEM',
    created_at        TIMESTAMP    NOT NULL DEFAULT NOW(),

    updated_last_by   VARCHAR(256)          DEFAULT NULL,
    updated_last_at   TIMESTAMP             DEFAULT NULL,

    deleted_by        VARCHAR(256)          DEFAULT NULL,
    deleted_at        TIMESTAMP             DEFAULT NULL,

    -- Parametric
    title              VARCHAR(256) NOT NULL,
    file              VARCHAR(256) NOT NULL

);
