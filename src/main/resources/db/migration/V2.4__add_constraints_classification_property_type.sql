ALTER TABLE inventory.classification_property_type
    ADD CONSTRAINT PK_classification_property_type PRIMARY KEY (id),
    ADD CONSTRAINT FK_property_type_id_to_property_type FOREIGN KEY (property_type_id) REFERENCES inventory.property_type(id),
    ADD CONSTRAINT FK_classification_id_to_classification FOREIGN KEY (classification_id) REFERENCES inventory.classification(id)
;
