CREATE TABLE inventory.classification_property_type
(
    -- Base
    id                BIGSERIAL    NOT NULL,
    classification_id BIGINT                DEFAULT NULL,
    property_type_id  BIGINT                DEFAULT NULL
);
